
resource "aws_kms_key" "cipher_text" {
	count = "${length(keys(var.plain_texts))}"
	description = "cipher_text"
}

resource "aws_kms_alias" "cipher_text" {
	count = "${length(keys(var.plain_texts))}"
	target_key_id = "${element(aws_kms_key.cipher_text.*.key_id, count.index)}"
	name = "${format("alias/%s", element(sort(keys(var.plain_texts)), count.index))}"
}

data "aws_kms_ciphertext" "cipher_text" {
	count = "${length(keys(var.plain_texts))}"
	key_id = "${element(aws_kms_key.cipher_text.*.key_id, count.index)}"
	plaintext = "${var.plain_texts[element(sort(keys(var.plain_texts)), count.index)]}"
}

output "cipher_texts"{
	value = "${zipmap(sort(keys(var.plain_texts)), data.aws_kms_ciphertext.cipher_text.*.ciphertext_blob)}"
}
