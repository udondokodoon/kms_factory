# kms_factory

## これはなに

terraformでAWS kmsのレコードを管理するツール

## 使い方

### terraform の s3 backend を設定する

backend.tf

```
provider "aws" {
	region = ""
}

terraform {
	backend "s3" {
		bucket = ""
		key    = ""
		region = ""
	}
}


data "terraform_remote_state" "kms" {
	backend = "s3"
	config {
		bucket = ""
		key    = ""
		region = ""
	}
}
```

### 暗号化したいテキストと キーを設定する

variables.tf

```
variable plain_texts {
	type = "map"
	default = {
		"key1" = "secret value1",
		"key2" = "secret value2",
	}
}
```

### terraform を実行する

```
$ terraform plan
$ terraform apply
```
