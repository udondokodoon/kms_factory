provider "aws" {
	region = ""
}

terraform {
	backend "s3" {
		bucket = ""
		key    = ""
		region = ""
	}
}


data "terraform_remote_state" "kms" {
	backend = "s3"
	config {
		bucket = ""
		key    = ""
		region = ""
	}
}
