variable plain_texts {
	type = "map"
	default = {
		"key1" = "secret value1",
		"key2" = "secret value2",
	}
}
